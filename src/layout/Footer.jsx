import { Box, Grid, GridItem } from "@chakra-ui/react";
import { FaGit, FaTwitter } from "react-icons/fa";
import MessageForm from "../components/MessageForm";
export default function Footer() {
  return (
    <Box position="fixed" bottom="0" width="100%">
      <MessageForm />

      <Grid
        gridTemplateColumns="auto 1fr"
        textAlign="center"
        alignItems="center"
        py="4px"
        px="30px"
        height="40px"
        bg="white"
      >
        <GridItem justifySelf="start">
          Websocket Chat using{" "}
          <a href="https://supabase.com/" target="_blank" rel="noreferrer">
            supabase
          </a>{" "}
          Realtime Postgres Changes. Credits:
          <a
            href="https://twitter.com/shwosner"
            target="_blank"
            rel="noreferrer"
          >
            @shwosner
          </a>
          (
          <a
            href="https://github.com/shwosner/realtime-chat-supabase-react"
            target="_blank"
            rel="noreferrer"
          >
            source fork
          </a>
          )
        </GridItem>
        <GridItem justifySelf="end">
          <a
            href="https://codeberg.org/gaddoz/websocket-react-chat/"
            target="_blank"
            rel="noreferrer"
          >
            <FaGit style={{ display: "inline" }} /> Source code
          </a>
        </GridItem>
      </Grid>
    </Box>
  );
}
