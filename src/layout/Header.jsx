import { Button, Grid, GridItem, Image } from "@chakra-ui/react";
import { m } from "framer-motion";
import { FaGithub } from "react-icons/fa";

import { useAppContext } from "../context/appContext";
import { pathResolver } from "../utils/path";
import NameForm from "./NameForm";
export default function Header() {
  const {
    username,
    setUsername,
    auth,
    randomUsername,
    isUserAuthenticated,
    setIsUserAuthenticated,
  } = useAppContext();
  return (
    <Grid
      templateColumns="30px max-content 1fr min-content"
      justifyItems="center"
      alignItems="center"
      bg="white"
      position="sticky"
      top="0"
      zIndex="10"
      borderBottom="20px solid #edf2f7"
    >
      <GridItem justifySelf="start" ml="2">
        <Image src={pathResolver("/logo.png")} height="30px" ml="2" />
      </GridItem>
      <GridItem justifySelf="start" m="2">
        <h1>websocket chat</h1>
      </GridItem>
      {isUserAuthenticated ? (
        <>
          <GridItem justifySelf="end" alignSelf="center" mr="4">
            Welcome <strong>{username}</strong>
          </GridItem>
          <Button
            marginRight="4"
            size="sm"
            variant="link"
            onClick={() => {
              const { error } = auth.signOut();
              if (error) return console.error("error signOut", error);
              const username = randomUsername();
              setUsername(username);
              setIsUserAuthenticated(false);
              localStorage.setItem("username", username);
            }}
          >
            Log out
          </Button>
        </>
      ) : (
        <>
          <GridItem justifySelf="end" alignSelf="end">
            <NameForm username={username} setUsername={setUsername} />
          </GridItem>
          <Button
            size="sm"
            marginRight="2"
            colorScheme="teal"
            rightIcon={<FaGithub />}
            variant="outline"
            onClick={() =>
              auth.signInWithOAuth({
                provider: "github",
              })
            }
          >
            Login
          </Button>
        </>
      )}
    </Grid>
  );
}
