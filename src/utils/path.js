export const baseUrl = () => {
  return import.meta.env.BASE_URL;
};

export const pathResolver = (url) => {
  const base = baseUrl();
  // import.meta.env.BASE_URL equals / when empty!!
  // in this case we just return url.
  if (base === "/") return url;
  // we join base url with required url.
  return base + url;
};
