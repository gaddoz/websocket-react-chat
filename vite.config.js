import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { loadEnv } from "vite";
import { createHtmlPlugin } from "vite-plugin-html";

export default ({ mode }) => {
  const env = loadEnv(mode, process.cwd());
  const BASE_URL = env.VITE_BASE_URL || "";
  return {
    base: `${BASE_URL}/`,
    plugins: [
      react(),
      createHtmlPlugin({
        minify: false,
        inject: {
          data: {
            baseurl: BASE_URL,
          },
        },
      }),
    ],
  };
};
